#!/bin/bash
# Avaliable environment:
#
# Download variable:
# WEB_URL=
GIT_URL="https://gitee.com/anolis/anolis-sys-tests.git"
DEP_PKG_LIST="python3"

build() {
    :
}

install() {
    python3 -m pip install -r $TONE_BM_BUILD_DIR/requirements.txt || {
        pip3 config set global.index-url http://mirrors.aliyun.com/pypi/simple/
        python3 -m pip install -r $TONE_BM_BUILD_DIR/requirements.txt --ignore-installed --trusted-host mirrors.aliyun.com
    }
    cp -a $TONE_BM_BUILD_DIR/* $TONE_BM_RUN_DIR
}

