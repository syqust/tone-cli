# pkg-smoke-test
## Description

Package smoke test, including cmd test, ldd test, install test, and service tests.

## Homepage
NA

## Version
v0.1

## Category
functional

## Results

```
attr: PASS
acl: PASS
```

## Manual Run

```
sh -x run_test.sh pkg-smoke-test
```

