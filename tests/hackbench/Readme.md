# hackbench
## Description
Hackbench  is  both  a benchmark and a stress test for the Linux kernel scheduler.
It's main job is to create a specified number  of  pairs  of schedulable entities
(either threads or traditional processes) which communicate via either sockets or
pipes and time how long it takes  for each pair to send data back and forth.

## Homepage
https://github.com/linux-test-project/ltp/blob/master/testcases/kernel/sched/cfs-scheduler/hackbench.c

## Version
v1.0

## Category
performance

## Parameters
- nr_task: number of pairs of schedulable entities (either threads or processes)
- mode: [process, thread]
- ipc: [pipe]
- datasize: Sets the amount of data to send in each message
- loop: How many messages each sender/receiver pair should send

## Results
```
Time: 44.434
```

## Manual Run
