#!/usr/bin/awk -f
BEGIN{ FS = "[: )\t/(]+";Throughput_eps=0 }
/transferred \(.* MiB\/sec\)/ { printf"Throughput_MB: %.2f MB/s\n",$(NF-3) }
/total time/ {total_time=$NF}
/time elapsed/ {total_time=$NF}
/total number of events:/ { printf("workload: %.2f\n"),$NF; events=$NF}
/events per second:/ { printf("Throughput_eps: %.2f \n",$NF);Throughput_eps=$NF }
/min:/ { printf"latency_min: %.2f ms\n",$NF }
/avg:/ { printf"latency_avg: %.2f ms\n",$NF }
/max:/ { printf"latency_max: %.2f ms\n",$NF }
/95th percentile:/ { printf"latency_95th: %.2f ms\n",$NF }
/events \(avg\/stddev\):/ { 
	printf"thread_events_avg: %.2f\nthread_events_stddev: %.2f\n",$(NF-1),$NF
}
/execution time \(avg\/stddev\):/ { 
	printf"exec_time_avg: %.4f s\nexec_time_stddev: %.2f\n",$(NF-1),$NF
}

END {
    if (Throughput_eps == 0)
        printf("Throughput_eps: %.2f \n", events/total_time)	
}
