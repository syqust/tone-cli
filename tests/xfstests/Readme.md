# xfstest
### Description
xfstests is a file system regression test suite which was originally developed by Silicon Graphics (SGI) for the XFS file system. Now  xfstests is used as a file system regression test suite for all of Linux major file systems: xfs, ext2, ext4, cifs, btrfs, f2fs, reiserfs, gfs, jfs, udf, nfs, and tmpfs

### Homepage

### Version
1.1.0

## Category
Functional

### Parameters
- _disk_type_ : hdd
- _nr_disk_ : 4 2
- _fs_ : ext4 xfs
- _test_ : add generic-all ext4-all overlay-all shared-all 
- _mkfsopt_ : bigalloc 
- _timeout_ : 4h 

### Results
generic/236: Pass
generic/237: Pass
generic/238: Conf
generic/239: Pass
generic/240: Pass
generic/241: Pass
generic/242: Conf
generic/243: Conf
generic/244: Pass
generic/245: Pass
generic/246: Pass
generic/247: Failed

### Manual Run

