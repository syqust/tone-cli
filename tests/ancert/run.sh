# Avaliable environment:
# TEST_MODE: run test mode, choice = 'certify', 'stress'
# TESTCASE_NAME_LIST: run tests case
# LTS_IP: run network need lts_ip
# Download variable:
# WEB_URL=
# GIT_URL=
TESTCASE_NAME_LIST=${TESTCASE_NAME_LIST:-""}

setup ()
{
    :
}

run ()
{
    cd $TONE_BM_RUN_DIR/ancert
    echo "Current working dir is `pwd`"

    echo "selected categroy is $category"
    option=""
    if [ -n "$TEST_MODE" ]; then
        echo "run test mode: $TEST_MODE"
        option="--mode $TEST_MODE"
    fi
    option="$option --category $category"
    if [ -n "$TESTCASE_NAME_LIST" ]; then
        echo "run tests case: $TESTCASE_NAME_LIST"
        option="$option --cases $TESTCASE_NAME_LIST"
    fi
    if [[ $option =~ "Network" ]]; then
        if [ -n "$LTS_IP" ]; then
            option="$option --lts_ip $LTS_IP"
        fi
    elif [[ $option =~ "System" ]]; then
        option="$option --single_mode"
    fi
    option="$option --report --tone"
    python3 ancert $option
}

teardown ()
{
    :
}

parse ()
{
    awk -f $TONE_BM_SUITE_DIR/parse.awk
}
