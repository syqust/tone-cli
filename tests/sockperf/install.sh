WEB_URL="https://ostester.oss-cn-hangzhou.aliyuncs.com/benchmarks/sockperf-3.10.tar.gz"

if echo "ubuntu debian uos kylin" | grep $TONE_OS_DISTRO; then
        DEP_PKG_LIST="autoconf automake gcc g++ libtool numactl"
else
        DEP_PKG_LIST="autoconf automake gcc gcc-c++ libtool numactl"
fi

build()
{
  cd $TONE_BM_BUILD_DIR/sockperf-3.10/
  ./autogen.sh
  ./configure --prefix="$TONE_BM_RUN_DIR"
  make
}

install()
{
  make install
}
