#!/usr/bin/env python
# -*- coding: utf-8 -*-


from utils.tests import TestEnvInit
from module_interfaces import TESTMODULE
# from utils.log import logger


class TestSetup(TESTMODULE):
    priority = 50

    def run(self, test_instance):
        if test_instance.config.subcommand == 'archive':
            return
        setup = TestEnvInit(test_instance.config)
        setup.test_info()
        setup.create_cache_path()
        setup.create_result_path()
        setup.create_run_path()
        setup.set_env()
